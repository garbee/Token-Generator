<?php

namespace Garbee\TokenGenerator;

class Generator
{
    /**
     * Generate a token of the given length.
     *
     * This code is based on an answer from StackOverflow.
     * http://stackoverflow.com/a/13733588/2604748
     * Using random_int to select the index over the custom function.
     *
     * @param int $length
     * @return string
     */
    public static function make(int $length) : string
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max)];
        }
        return $token;
    }
}
